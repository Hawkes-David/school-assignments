<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-28
 * Purpose: A web form to gather input about albums for the vinyl creator web application
 */

include "classes/button.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Vinyl Creator - Add Album</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>The Vinyl Creator</h1>
            </div>
            <?php //Start of form ?>
            <form action="process.php" method="post">
                <h3>Please add your favorite album to the vinyl creator!</h3>
                <?php //inputs for album title, artist, publisher, genre, # of tracks, and release year ?>
                <div class="form-group row">
                    <label for="inputAlbumTitle" class="col-sm-2 col-form-label">Album Title<span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputAlbumTitle" name="inputAlbumTitle" placeholder="Album Title">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputArtist" class="col-sm-2 col-form-label">Artist <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputArtist" name="inputArtist" placeholder="Artist">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPublisher" class="col-sm-2 col-form-label">Publisher <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPublisher" name="inputPublisher" placeholder="Publisher">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputGenre" class="col-sm-2 col-form-label">Genre <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputGenre" name="inputGenre" placeholder="Genre">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputNumberOfTracks" class="col-sm-2 col-form-label">Number of Tracks <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="inputNumberOfTracks" name="inputNumberOfTracks" placeholder="Number of Tracks">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputReleaseYear" class="col-sm-2 col-form-label">Release Year <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="inputReleaseYear" name="inputReleaseYear" placeholder="Release Year">
                    </div>
                </div>
                <?php
                    //Submit album button, using the button class
                    $submitButton = new Button();
                    $b1->buttonName = "submit";
                    $b1->buttonValue = "Add Album";
                    $b1->buttonStyle = "font-family:sans-serif";
                    $b1->display();
                ?>
            </form>
        </div>
    </body>
</html>
