<?php

/**
 * Class Button: Created in class, used to incorporate into assignment.
 */

class Button {

	private $buttonName;
	private $buttonValue;
	private $buttonStyle;
	
	function __construct() {
		$this->buttonName = "";
		$this->buttonValue = "";
		$this->buttonStyle = "";
	}
	
	function __set($whichOne,$whatVal) {
		$this->$whichOne = $whatVal;
	}
	
	function __get($whichItem) {
		return $this->$whichItem;
	}
	
	function display() {
		
		echo "<input type='submit' value='". $this->buttonValue . "' style='". $this->buttonStyle . "' name='". $this->buttonName. "' />";
	}
}

$b1 = new Button();
$b1->buttonName = "submit";
$b1->buttonValue = "Boo!";
$b1->buttonStyle = "font-family:sans-serif";
//$b1->display();

//example of using the get!
//echo "<span style='color:red;'>$b1->buttonValue</span>";

?>