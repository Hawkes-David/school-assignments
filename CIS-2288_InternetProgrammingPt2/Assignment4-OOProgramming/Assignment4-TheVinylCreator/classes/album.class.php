<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-28
 * Purpose: This class represents a music album
 */

class Album {

    //private attributes of an album class
    private $albumTitle;
    private $albumArtist;
    private $albumPublisher;
    private $albumGenre;
    private $albumNumberOfTracks;
    private $albumReleaseYear;

    //constructor to create album objects
    function __construct() {
        $this->albumTitle = "";
        $this->albumArtist = "";
        $this->albumPublisher = "";
        $this->albumGenre = "";
        $this->albumNumberOfTracks = "";
        $this->albumReleaseYear = "";

        echo "<h3>Album Created</h3>";
    }

    //magic function to set the private attributes
    function __set($attribute,$attributeValue) {
        $this->$attribute = $attributeValue;
    }

    //magic function to get the private attributes
    function __get($attributeValue) {
        return $this->$attributeValue;
    }

    //function to print all the attributes of an album to a web page
    function printDetails() {

        echo "<p><span id=\"label\">Album Title: </span> $this->albumTitle <br>
              <span id=\"label\">Artist: </span> $this->albumArtist <br>
              <span id=\"label\">Publisher: </span> $this->albumPublisher <br>
              <span id=\"label\">Genre: </span> $this->albumGenre <br>
              <span id=\"label\">Number of Tracks: </span> $this->albumNumberOfTracks <br>
              <span id=\"label\">Release Year: </span> $this->albumReleaseYear </p><br>";
    }
}
?>