<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-28
 * Purpose: To process, validate and display a album object for the vinyl creator web app.
 */

include "classes/album.class.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Vinyl Creator - Processing</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>The Vinyl Creator</h1>
            </div>
            <?php
                //Check to see if the user navigated to this page by clicking the submit button.
                if (isset($_POST{'inputAlbumTitle'})){
                    //Declare variables and constants
                    $title = htmlspecialchars($_POST{'inputAlbumTitle'});
                    $artist = htmlspecialchars($_POST{'inputArtist'});
                    $publisher = htmlspecialchars($_POST{'inputPublisher'});
                    $genre = htmlspecialchars($_POST{'inputGenre'});
                    $tracks = htmlspecialchars($_POST{'inputNumberOfTracks'});
                    $releaseYear = htmlspecialchars($_POST{'inputReleaseYear'});

                    // Required field names
                    $required = array('inputAlbumTitle', 'inputArtist', 'inputPublisher', 'inputGenre', 'inputNumberOfTracks', 'inputReleaseYear');

                    // Loop over field names, make sure each one exists and is not empty
                    $error = false;
                    foreach($required as $field) {
                        if (empty($_POST[$field])) {
                            $error = true;
                        }
                    }
                    //If boolean flag is tripped get user to return to the previous page.
                    if ($error) {
                        echo "<h3 class=\"text-warning\">You must enter a value for each required field. Required fields are marked by an asterisk (*)</h3>";
                        echo "<a href='addAlbum.php'>Return to previous page</a></div></body></html>";
                        //exit the script without executing anything else
                        exit;

                        //Validate the year format
                    } elseif ($releaseYear < 1000 || $releaseYear > 9999){
                        echo "<h3 class=\"text-warning\">The year must follow the 'YYYY' format. (Example: 2018)</h3>";
                        echo "<a href='addAlbum.php'>Return to previous page</a></div></body></html>";
                        //exit the script without executing anything else
                        exit;

                        //Display the page
                    } else {
                        //Create the album object
                        $album = new Album();

                        //Set the attributes
                        $album -> albumTitle = $title;
                        $album -> albumArtist = $artist;
                        $album -> albumPublisher = $publisher;
                        $album -> albumGenre = $genre;
                        $album -> albumNumberOfTracks = $tracks;
                        $album -> albumReleaseYear = $releaseYear;

                        //Display the album
                        $album -> printDetails();

                        echo "<a href='addAlbum.php'>Add another album!</a>";
                    }

                } else {
                    //Redirect to the add album page
                    echo "<h3 class=\"text-warning\">Something went wrong!</h3>";
                    echo "<a href='addAlbum.php'>Add your favorite album here</a></div></body></html>";
                    //exit the script without executing anything else
                    exit;
                }
            ?>
        </div>
    </body>
</html>