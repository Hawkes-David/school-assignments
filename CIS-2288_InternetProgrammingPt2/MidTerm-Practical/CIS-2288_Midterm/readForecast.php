<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-17
 * Purpose: This file is updated from a text file to display weather details
 */

?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport"
			  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="css/bootstrap.css" rel="stylesheet">
		<title>Forecast</title>
		<style>
			.container-fluid {
				/* This class is used as is from bootstrap. No changes required. */
			}
			/* This class creates the blue boxes of the correct width for each day. */
			.bg-primary {
				width:500px;
				height: auto;
				min-height: 150px;
				padding:10px;
				margin-bottom:2px;
			}
			/* This sets the paragraphs away from the left margin */
			.bg-primary p {
				padding-left:90px;
			}
			/* This floats the image to the left and sets the size. */
			.forecastImage {
				margin-right: 10px;
				width: 80px;
				float:left;
			}
			h5 {
				background-color: yellow;
				width:500px;
				padding: 10px;
			}
		</style>
	</head>
	<body>
		<div class="container-fluid">
            <?php
            //set the default timezome
            date_default_timezone_set('America/Halifax');

            //get the forecast details from the text file
            $forecastDetails = file("forecastData/forecast.txt");

            //Variables to calculate average
            $totalTempForAverageHigh = 0;
            $totalTempForAverageLow = 0;

            //find how many days are in the file to be displayed
            $numberOfDaysForecast = count($forecastDetails);

            //Display this message if the file cannot be loaded or if the file is empty and end the script
            if($numberOfDaysForecast == 0 || $forecastDetails == false){
                echo "<h1>Forecast is unavailable. Our web team has been notified.</h1><br>
                      <p>Current as of ".date('m/d/Y')."</p></div></body></html>";
                exit;
            }

            //for loop to get info about each day in the text file.
            for($i=0; $i<$numberOfDaysForecast; $i++) {
                //create an array for each day
                $day[$i] = explode(";", $forecastDetails[$i]);

                //This will set determine what code to use depending on the contents of the text file
                if ($day[$i][8] == "celsius") {
                    $degreeCode = "&#x2103";
                } elseif ($day[$i][8] == "fahrenheit") {
                    $degreeCode = "&#8457";
                }

                //add each temperature to the average variable
                $totalTempForAverageHigh += intval($day[$i][4]);
                $totalTempForAverageLow += intval($day[$i][5]);

                //switch will determine the month based on the contents of the text file.
                $month = $day[$i][2];
                switch ($month) {
                    case "1":
                        $month = "January";
                        break;
                    case "2":
                        $month = "February";
                        break;
                    case "3":
                        $month = "March";
                        break;
                    case "4":
                        $month = "April";
                        break;
                    case "5":
                        $month = "May";
                        break;
                    case "6":
                        $month = "June";
                        break;
                    case "7":
                        $month = "July";
                        break;
                    case "8":
                        $month = "August";
                        break;
                    case "9":
                        $month = "September";
                        break;
                    case "10":
                        $month = "October";
                        break;
                    case "11":
                        $month = "November";
                        break;
                    case "12":
                        $month = "December";
                        break;
                    default:
                        $month = $day[$i][2];
                }

            }
            //calculate the average temp
            $averageHigh = $totalTempForAverageHigh / $numberOfDaysForecast;
            $averageLow = $totalTempForAverageLow / $numberOfDaysForecast;
            ?>


			<h3>5 Day Forecast for <?php echo $day[0][9] ?></h3>
            <?php //For loop to display each day ?>
            <?php for($i=0; $i<$numberOfDaysForecast; $i++){ ?>
			<div class='bg-primary'>
                <?php //Display the date ?>
				<h4><?php echo $day[$i][0].", ".$month." ".$day[$i][1].", ".$day[$i][3] ?></h4>
                <?php //Display the images ?>
				<img src='images/
				<?php
                    if($day[$i][6] == "overcast"){
                        echo "cloudy";
                    } else {
                        echo $day[$i][6];
                    }
                ?>.jpg' class='forecastImage'>
                <?php //Display the weather summary ?>
				<p><?php echo $day[$i][7]?></p>
                <?php  //Display the highs and lows with degree code?>
				<p>High: <?php echo $day[$i][4].$degreeCode ?> Low: <?php echo $day[$i][5].$degreeCode ?></p>
			</div>
            <?php }?>
            <?php //Display the averages ?>
			<h5>Weekly Temperature Averages: High: <?php echo $averageHigh.$degreeCode ?> Low: <?php echo $averageLow.$degreeCode ?></h5>
            <p>Current as of <?php echo date('m/d/Y'); ?></p>

			<!-- this code will be used in the event the data file is not found or is empty
			<h2>Forecast is unavailable. Our web team has been notified.</h2>-->
        </div>
	</body>
</html>