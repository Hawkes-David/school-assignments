<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-12
 * Purpose: To reset a file containing order details to empty.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Reset Orders</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/customVeg.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>Reset Orders</h1>
            </div>
            <?php
                //find the path to the file.
                $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
                $pathToFile = $DOCUMENT_ROOT."/../veggie-orders.txt";

                //Reset the file so that it is empty
                file_put_contents($pathToFile, "");

                //Redirect the user to the viewOrders page
                header('Location: viewOrders.php')
            ?>
        </div>
    </body>
</html>
