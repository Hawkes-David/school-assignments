<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-12
 * Purpose: An order form for an organic vegetable farm.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Vegetable Ordering</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/customVeg.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>Organic Vegetable Ordering Form</h1>
            </div>
            <?php //Start of form ?>
            <form action="processVeggies.php" method="post">
                <?php //inputs for name, email and address ?>
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Name <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAddress" class="col-sm-2 col-form-label">Address <span id="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputAddress" name="inputAddress" placeholder="Address">
                    </div>
                </div>
                <?php //Customer can enter the amount of potatoes, carrots, and cauliflower they wish to order ?>
                <div class="form-group">
                    <label for="potatoesQty">Potatoes - 5 lbs ($6.00) </label>
                    <select id="potatoesQty" name="potatoesQty" class="form-control">
                        <?php
                        for($i=0;$i<=30;$i++)
                        {
                            echo "<option value=\"$i\">$i</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="carrotsQty">Carrots - 3 lbs ($3.75) </label>
                    <select id="carrotsQty" name="carrotsQty" class="form-control">
                        <?php
                        for($i=0;$i<=30;$i++)
                        {
                            echo "<option value=\"$i\">$i</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cauliflowerQty">Cauliflower ($4.00 each) </label>
                    <select id="cauliflowerQty" name="cauliflowerQty" class="form-control">
                        <?php
                        for($i=0;$i<=30;$i++)
                        {
                            echo "<option value=\"$i\">$i</option>";
                        }
                        ?>
                    </select>
                </div>
                <?php //Submit order button ?>
                <button type="submit" class="btn btn-primary" id="submitButton">Place Your Order!</button>
            </form>
        </div>
    </body>
</html>