<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-12
 * Purpose: A page that reads order deatils from a file for an administrator of an organic vegetable farm.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>View Orders</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/customVeg.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>Today's Pending Orders</h1>
            </div>
            <?php
                //Find the path to the file
                $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
                $pathToFile = $DOCUMENT_ROOT."/../veggie-orders.txt";

                //Open the file
                $fp = fopen($pathToFile, 'r');

                //If the file cannot be opened display it as such
                if (!$fp) {
                    echo "<p><strong>Unable to find file location.</strong></p>";
                    exit("</div></body></html>");
                }

                //If the file is empty, display a message
                if (filesize($pathToFile)==0) {
                    echo "<h4>There are no pending orders.</h4>";
                    echo "<p>You can place an order <a href='orderVegetables.php'>here.</a></p></div></body></html>";

                    //Else read the file and put the contents into a table
                } else {
                    echo "<table class=\"table table-sm\">";
                    while (!feof($fp)) {
                        $order= fgets($fp);
                        echo "<tr><td>" . str_replace('\t','</td><td>',$order) . '</td></tr>';
                    }
                    echo "</table>";

                    // Close the file
                    fclose($fp);

                    //Link to the resetOrders page.
                    echo "<p>Would you like to reset the orders? <a href='resetOrders.php'>Click Here</a>";
                }
            ?>
        </div>
    </body>
</html>
