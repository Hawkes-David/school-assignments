<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-12
 * Purpose: The output page for an organic vegetable farm. This also will write order details to a file.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Vegetable Processing</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/customVeg.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>Order Confirmation</h1>
            </div>
            <?php
                //Check to see if the user navigated to this page by clicking the submit button.

                //If yes
                if(isset($_POST{'inputName'})){

                    //Declare variables and constants
                    $name = htmlspecialchars($_POST{'inputName'});
                    $email = htmlspecialchars($_POST{'inputEmail'});
                    $address = htmlspecialchars($_POST{'inputAddress'});
                    $potatoesQty = htmlspecialchars($_POST['potatoesQty']);
                    $carrotsQty = htmlspecialchars($_POST['carrotsQty']);
                    $cauliflowerQty = htmlspecialchars($_POST['cauliflowerQty']);
                    define('PRICE_POTATOES', 6.00);
                    define('PRICE_CARROTS', 3.75);
                    define('PRICE_CAULIFLOWER', 4.00);
                    define('DELIVERY', 5.00);
                    define('TAX_RATE', .15);
                    $totalItems = 0;
                    $totalCostBeforeTax = 0.00;
                    $delivery = 0.00;
                    $amountOfTax = 0.00;
                    $totalCostAfterTax = 0.00;

                    // Required field names
                    $required = array('inputName', 'inputEmail', 'inputAddress');

                    // Loop over field names, make sure each one exists and is not empty
                    $error = false;
                    foreach($required as $field) {
                        if (empty($_POST[$field])) {
                            $error = true;
                        }
                    }
                    //If boolean flag is tripped get user to return to the previous page.
                    if ($error) {
                        echo "<p class=\"text-warning\">You must enter a value for each required field. Required fields are marked by an asterisk (*)</p>";
                        echo "<a href='orderVegetables.php'>Return to previous page</a></div></body></html>";
                        //exit the script without executing anything else
                        exit;
                        //Else make sure something was ordered
                    } else {
                        if ($potatoesQty == 0 && $carrotsQty == 0 && $cauliflowerQty == 0) {
                            echo "<p class=\"text-warning\">You must order at least one item. <a href='orderVegetables.php'>Try again</a></p></div></body></html>";
                            //exit the script without executing anything else
                            exit;
                        } else {
                            //Set default timezone and display when the order was placed.
                            date_default_timezone_set('America/Halifax');
                            // set date for order record in file
                            $time = date('g:ia');
                            $month =  date('F');
                            $day =  date('j');
                            $year = date('Y');

                            //display order details
                            echo "<p><strong>Your order is as follows: </strong></p>";
                            echo "<p><strong>Name: </strong>".$name."</p>";
                            echo "<p><strong>Email: </strong>".$email."</p>";
                            echo "<p><strong>Address: </strong>".$address."</p>";
                            echo "<p><strong>Potatoes: </strong>".$potatoesQty."</p>";
                            echo "<p><strong>Carrots: </strong>".$carrotsQty."</p>";
                            echo "<p><strong>Cauliflower: </strong>".$cauliflowerQty."</p>";
                            $totalItems = $potatoesQty + $carrotsQty + $cauliflowerQty;
                            echo "<p><strong>Items ordered: </strong>".$totalItems."</p>";

                            //Calculate costs
                            $totalCostBeforeTax = ($potatoesQty * PRICE_POTATOES) + ($carrotsQty * PRICE_CARROTS) + ($cauliflowerQty * PRICE_CAULIFLOWER);

                            if($totalCostBeforeTax > 50){
                                $delivery = 0.00;
                            } else{
                                $delivery = DELIVERY;
                            }

                            $amountOfTax = ($totalCostBeforeTax + $delivery) * TAX_RATE;
                            $totalCostAfterTax = $totalCostBeforeTax + $delivery + $amountOfTax;

                            //Table to display costs
            ?>
                            <br>
                            <table class="table table-sm">
                                <tr>
                                    <th scope="row">Subtotal:</th>
                                    <td><?php echo "$".number_format($totalCostBeforeTax, 2);?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Delivery:</th>
                                    <td><?php echo "$".number_format($delivery, 2);?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Provincial Sales Tax:</th>
                                    <td><?php echo "$".number_format($amountOfTax, 2);?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Total:</th>
                                    <td><?php echo "$".number_format($totalCostAfterTax, 2);?></td>
                                </tr>
                            </table>
            <?php
                            //Display the time the order was processed
                            echo "<p id='date'>Order processed at ".date('g:ia')." on ".date('F j, Y').".</p>";
                            //Link to viewOrders page.
                            echo "<p><a href='viewOrders.php'>View all the days orders</a></p>";

                            //The output string that will be written to a file so to save the order details.
                            $outputString = $time."\t".$month."\t".$day."\t".$year."\t".$potatoesQty." potatoes \t".$carrotsQty." carrots\t"
                                .$cauliflowerQty." cauliflower\t\$".number_format($totalCostBeforeTax, 2)
                                ."\t\$".number_format($delivery, 2)."\t\$".number_format($amountOfTax, 2)
                                ."\t\$".number_format($totalCostAfterTax, 2)."\t".$name."\t".$email."\t".$address."\r\n";

                            //Create file path
                            $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
                            //Create the file if it does not exist, open it if it does.
                            $fp = fopen("$DOCUMENT_ROOT/../veggie-orders.txt", 'a+');
                            //Lock the file
                            flock($fp, LOCK_EX);
                            //Display a message if there is an issue with the file
                            if (!$fp) {
                                echo "<p><strong> Your order could not be processed at this time. Please try again later.</strong></p></body></html>";
                                exit;
                            }
                            //Write to, unlock and close the file
                            fwrite($fp, $outputString, strlen($outputString));
                            flock($fp, LOCK_UN);
                            fclose($fp);
                        }
                    }
                    //If the user did not click the submit buttom, give them options.
                } else {
                    echo "<p class=\"text-warning\">Something went wrong!</p>";
                    echo "<a href='orderVegetables.php'>Place an order here</a></div></body></html>";
                    //exit the script without executing anything else
                    exit;
                }
            ?>
        </div>
    </body>
</html>
