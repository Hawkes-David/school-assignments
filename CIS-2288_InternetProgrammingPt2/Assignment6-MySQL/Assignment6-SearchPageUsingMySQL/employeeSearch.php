<?php
/**
 * Author: David Hawkes
 * Date: 2018-11-25
 * Purpose: A page that will allow users to search for employees within the CIS2288 database.
 */
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee Search</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="employeeArea">
            <div class="page-header">
                <h1>CIS2288 Database</h1>
            </div>
            <form action="employeeSearchResults.php" method="get">
                <fieldset class="form-group">
                    <legend>Employee Search</legend>
                    <div class="form-group">
                        <label for="inputFirstName">First Name</label>
                        <input type="text" class="form-control" id="inputFirstName" name="inputFirstName" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <label for="inputLastName">Last Name</label>
                        <input type="text" class="form-control" id="inputLastName" name="inputLastName" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <label for="resultsLimit">The Number of Results Displayed:</label>
                        <select id="resultsLimit" name="resultsLimit" class="form-control">
                            <option value="2">2</option>
                            <option selected value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="all">All</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="orderByAttribute">Order Results By:</label>
                        <select id="orderByAttribute" name="orderByAttribute" class="form-control">
                            <option value="EMP_ID">Employee ID</option>
                            <option value="FIRST_NAME">First Name</option>
                            <option selected value="LAST_NAME">Last Name</option>
                            <option value="START_DATE">Start Date</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="orderAscending">Display Results:</label>
                        <select id="orderAscending" name="orderAscending" class="form-control">
                            <option selected value="true">Ascending</option>
                            <option value="false">Descending</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary" id="submitButton">Search</button>
                </fieldset>
            </form>
        </div>
    </body>
</html>
