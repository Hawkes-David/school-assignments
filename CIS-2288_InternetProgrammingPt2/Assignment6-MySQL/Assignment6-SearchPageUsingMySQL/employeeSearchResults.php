<?php
/**
 * Author: David Hawkes
 * Date: 2018-11-25
 * Purpose: Display the results of a employee database query based on user inputs.
 */
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee Search Results</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="employeeArea">
            <div class="page-header">
                <h1>CIS2288 Database</h1>
            </div>
            <?php
                //Check to see if it's the first time the user has loaded the page.
                if (!isset($_GET{'inputFirstName'})){

                    //If so, display a warning message and direct them to the search site.
                    echo "<p class=\"text-warning\">Something went wrong!</p>";
                    echo "<a href='employeeSearch.php'>Search for an Employee Here</a></div></body></html>";
                    //exit the script without executing anything else
                    exit;

                } else {

                    //Create a database connection
                    @ $db = new mysqli('localhost', 'root', '', 'cis2288');

                    //If the connection had issues, display warning
                    if (mysqli_connect_errno()) {
                        echo 'Error: Could not connect to database.  Please try again later.</div></body></html>';
                        //exit the script without executing anything else
                        exit;
                    }

                    //Variables
                    $firstName = ($_GET['inputFirstName']);
                    $lastName = ($_GET['inputLastName']);
                    $resultsLimit = ($_GET['resultsLimit']);
                    $orderByAttribute = ($_GET['orderByAttribute']);
                    $orderAscending = ($_GET['orderAscending']);

                    //Params for bind_param function
                    $firstNameParam = "%{$_GET['inputFirstName']}%";
                    $lastNameParam = "%{$_GET['inputLastName']}%";
                    $bindParam = "";

                    //Number of variables needed
                    $numberOfParamVars = 1;

                    //Starting query
                    $query = "SELECT EMP_ID, FIRST_NAME, LAST_NAME, START_DATE FROM employee";

                    //Check to see which inputs the user has filled out
                    if ($firstName != "" && $lastName == ""){

                        //add to the query string
                        $query .= " WHERE FIRST_NAME LIKE ?";

                        //set the param
                        $bindParam = "%{$_GET['inputFirstName']}%";

                    } elseif ($firstName == "" && $lastName != ""){

                        $query .= " WHERE LAST_NAME LIKE ?";

                        $bindParam = "%{$_GET['inputLastName']}%";

                    } elseif ($firstName != "" && $lastName != ""){

                        $query .= " WHERE FIRST_NAME LIKE ? OR LAST_NAME LIKE ?";

                        //Set the number of variables required
                        $numberOfParamVars = 2;

                    } elseif ($firstName == "" && $lastName == ""){

                        $numberOfParamVars = 0;
                    }

                    //add the order by info to the query
                    $query .= " ORDER BY ". $orderByAttribute;

                    //adds descending if the user selects so (default is ascending)
                    if ($orderAscending == "false"){
                        $query .= " DESC";
                    }

                    //Set a limit if the user did not select all
                    if ($resultsLimit != "all"){
                        $query .= " LIMIT " . $resultsLimit;
                    }

                    //Query the database, different methods are used depending on the user inputs.
                    if ($numberOfParamVars == 0){

                        //No input from user, safe to do a simple query.
                        $result = $db->query($query);

                    } elseif ($numberOfParamVars == 2){

                        //The user has used both first and last name inputs, need to use a prepare statement for security with two variable parameters
                        if ($statement = $db->prepare($query)){
                            $statement->bind_param("ss", $firstNameParam, $lastNameParam);
                            $statement->execute();
                            $result = $statement->get_result();
                            $statement->close();
                        }
                    } elseif ($numberOfParamVars == 1){

                        //The user has used one of either the first or last name inputs, need to use a prepare statement for security with one variable parameters
                        if ($statement = $db->prepare($query)) {
                            $statement->bind_param("s", $bindParam);
                            $statement->execute();
                            $result = $statement->get_result();
                            $statement->close();
                        }
                    }

                    echo "<h2>Search Results</h2>";

                    //Check to see if the query has any results
                    if ($result->num_rows > 0){
                        //if so create a table.
            ?>
            <table class="resultsTable">
                <thead>
                    <tr>
                        <th class="firstCol">Employee ID</th>
                        <th class="middleCol">First Name</th>
                        <th class="middleCol">Last Name</th>
                        <th class="lastCol">Start Date</th>
                    </tr>
                </thead>
                <tbody>
            <?php
                        // use a while loop to loop through all the results in the $result object
                        while($row = $result->fetch_assoc()){
            ?>
                    <tr>
                        <td><?php echo $row['EMP_ID'] ?></td>
                        <td><?php echo $row['FIRST_NAME'] ?></td>
                        <td><?php echo $row['LAST_NAME'] ?></td>
                        <td><?php echo $row['START_DATE'] ?></td>
                    </tr>
            <?php
                        }
            ?>
                </tbody>
            </table>
            <?php
                    } else {
                        //If there are no results that ,atch the query.
                        echo "<p>Sorry your search has not yielded any results.</p>";
                    }
                    echo "<br><a href=\"employeeSearch.php\">Back to the Search Page</a>";

                    //this frees up memory on the server
                    $result->free();

                    //disconnect the connection to the DB
                    $db->close();
                }
            ?>
        </div>
    </body>
</html>
