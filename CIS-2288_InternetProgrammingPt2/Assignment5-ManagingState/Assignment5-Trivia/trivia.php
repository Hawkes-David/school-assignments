<?php
/**
 * Author: David Hawkes
 * Date: 2018-11-13
 * Purpose: A trivia game to demonstrate how sessions can manage the state of web apps.
 */
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Trivia Game</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <div class="page-header">
                <h1>Trivia</h1>
            </div>
            <?php
                //start the session
                session_start();

                //get the contents of the text file (questions and answers)
                $fileContents = file("triviaQuestions/triviaQuestions.txt");

                //get a count of questions
                $numberOfQuestions = count($fileContents);

                //Seperate the questions from the answers
                for($i=0; $i<$numberOfQuestions; $i++){
                    $question[$i] = explode("\t", $fileContents[$i]);
                }

                //A function to calculate a percentage.
                function calculatePercentage($number, $divider){

                    $percentage = ( $number / $divider ) * 100;
                    echo round($percentage,2);

                }

                //function to determine what class to display for a correct/incorrect answer.
                function determineClass($yesOrNo){

                    if ($yesOrNo == "YES"){
                        echo "correct";
                    } else {
                        echo "incorrect";
                    }

                }

                //This will end the session if the user clicks the restart link
                if (isset($_GET['action'])){

                    if ($_GET['action'] == 'restart'){

                        // this gets rid of the session cookie that php puts on the users machine
                        if (ini_get("session.use_cookies")) {
                            $params = session_get_cookie_params();
                            setcookie(session_name(), '', time() - 42000,
                                $params["path"], $params["domain"],
                                $params["secure"], $params["httponly"]
                            );
                        }

                    }
                    //redirect to the trivia.php page
                    header('location: trivia.php');
                }

                //This will provide functionality if the submit button has been clicked.
                if (isset($_POST['inputAnswer'])){

                    //declare some variables
                    $theGuess = $_POST['inputAnswer'];
                    $theAnswer = $question[$_SESSION['questionsAsked']][1];
                    $theGuess = trim($theGuess);
                    $theAnswer = trim($theAnswer);
                    $questionCount = $_SESSION['questionCount'];

                    //Prompt the user with a message if they enter an empty value
                    if (empty($theGuess)){
                        echo "<p class=\"text-warning\">Your answer cannot be blank.</p>";
                    } else {

                        //Increase the count
                        $questionCount++;
                        $_SESSION['questionCount'] = $questionCount;

                        //Add the guess to an array to be displayed in a table later
                        array_push($_SESSION['answerArray'], $theGuess);

                        //Increase this variable as well.
                        $_SESSION['questionsAsked'] += 1;

                        //If the guess matches the answer (case insensitive)
                        if (strcasecmp($theGuess, $theAnswer) == 0){
                            //Increase the total for correct answers
                            $_SESSION['correctAnswers'] += 1;

                            //Depending on whether or not the guess was correct, this code will update an array to be used to
                            // determine the background color of the results table
                            array_push($_SESSION['wasItCorrectArray'], "YES");
                        } else {
                            array_push($_SESSION['wasItCorrectArray'], "NO");
                        }

                    }
                    //This code will run if the user has no session or if it is the first time the trivia game is loaded.
                } else {

                    //This code will set some session variables to be used in the form
                    if (!isset($_SESSION['questionCount'])){

                        $_SESSION['questionCount'] = 1;
                        $_SESSION['questionsAsked'] = 0;
                        $_SESSION['answerArray'] = [];
                        $_SESSION['wasItCorrectArray'] = [];
                        $_SESSION['correctAnswers'] = 0;

                    }
                }
                        //Display the questions if there are more to be asked.
                        if ($_SESSION['questionsAsked'] < $numberOfQuestions){
                    ?>
                            <form action="trivia.php" method="post">
                                <h3>Question <?php echo $_SESSION['questionCount'] ?> of <?php echo $numberOfQuestions ?></h3>
                                <?php //The input for each question ?>
                                <div class="form-group">
                                    <label for="inputAnswer"><?php echo $question[$_SESSION['questionsAsked']][0]?></label>
                                    <input type="text" class="form-control" id="inputAnswer" name="inputAnswer" placeholder="Answer">
                                </div>
                                <?php //Submit the answer ?>
                                <button type="submit" class="btn btn-primary" id="submitButton">Next Question</button>
                                <br><br><a href="trivia.php?action=restart">Restart The Game</a>
                            </form>
                    <?php
                        } else {
                            //Display the results in a table
                    ?>      <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Question</th>
                                        <th scope="col">Correct Answer</th>
                                        <th scope="col">Entered Answer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i=0; $i<$numberOfQuestions; $i++){ ?>
                                    <tr class="<?php determineClass($_SESSION['wasItCorrectArray'][$i]) ?>">
                                        <td><?php echo ($i + 1) ?></td>
                                        <td><?php echo $question[$i][0] ?></td>
                                        <td><?php echo $question[$i][1] ?></td>
                                        <td><?php echo $_SESSION['answerArray'][$i] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <br>
                            <p> You got <?php echo $_SESSION['correctAnswers'] ?> out of <?php echo $numberOfQuestions ?> correct. That is <?php calculatePercentage($_SESSION['correctAnswers'], $numberOfQuestions) ?>%</p>
                            <a href="trivia.php?action=restart">Restart The Game</a>
                    <?php } ?>
        </div>
    </body>
</html>
