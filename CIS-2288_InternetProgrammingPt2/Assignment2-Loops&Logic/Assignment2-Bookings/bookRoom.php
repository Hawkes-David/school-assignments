<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-02
 * Purpose: A web application to allow users to book cottages and hotel rooms online.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Room Bookings</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="bookingForm">
            <div class="page-header">
                <h1>Booking Form</h1>
            </div>
            <?php //Image of cottage  ?>
            <img src="media/cottageRental.jpg" class="img-responsive img-rounded center" alt="Cottage on the water">
            <br><br>
            <?php //Start of form ?>
            <form action="processBooking.php" method="post">
                <?php //All the inputs for the form ?>
                <div class="form-group">
                    <label for="firstName">First Name <span id="required">*</span></label>
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="lastName">Last Name <span id="required">*</span></label>
                    <input type="text" class="form-control" id="lastNameName" name="lastName" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="address">Address <span id="required">*</span></label>
                    <textarea class="form-control rounded-0" id="address" name="address" rows="1" placeholder="Address"></textarea>
                </div>
                <div class="form-group">
                    <label for="postalCode">Postal Code <span id="required">*</span></label>
                    <input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Postal Code">
                </div>
                <div class="form-group">
                    <label for="numberOfNights">How many nights will you be staying? ($100 per night) <span id="required">*</span></label>
                    <select id="numberOfNights" name="numberOfNights" class="form-control">
                        <option value="">-Select One-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="numberOfAdults">How many adults will be staying? (Minimum of 1) <span id="required">*</span></label>
                    <input type="text" class="form-control" id="numberOfAdults" name="numberOfAdults" placeholder="0">
                </div>
                <div class="form-group">
                    <label for="numberOfChildren">How many children will be staying?</label>
                    <input type="text" class="form-control" id="numberOfChildren" name="numberOfChildren" placeholder="0">
                </div>
                <div class="form-group">
                    <b>Will you be bringing a pet? (Additional $50 charge)</b><br>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="form-check-input" name="petOption" id="Yes" value="50" />Yes
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="submitButton">Book Room</button>
            </form>
            <br><br>
        </div>
    </body>
</html>
