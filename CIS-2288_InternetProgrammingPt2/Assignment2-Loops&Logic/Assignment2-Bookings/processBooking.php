<?php
/**
 * Author: David Hawkes
 * Date: 2018-10-02
 * Purpose: The output page for a room booking web application.
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Process Bookings</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div id="bookingResults">
            <div class="page-header">
                <h1>Booking Confirmation</h1>
            </div>
            <?php
                //Declare variables and constants
                $firstName = htmlspecialchars($_POST{'firstName'});
                $lastName = htmlspecialchars($_POST{'lastName'});
                $address = htmlspecialchars($_POST{'address'});
                $postalCode = htmlspecialchars($_POST{'postalCode'});
                $nights = htmlspecialchars($_POST{'numberOfNights'});
                $adults = htmlspecialchars($_POST{'numberOfAdults'});
                $children = htmlspecialchars($_POST{'numberOfChildren'});
                define('SALES_TAX', 1.15);
                $petOption = 0;
                $totalForNights = 0;
                $totalDueBeforeTax = 0;
                $totalDueAfterTax = 0;
                $totalTaxAmount = 0;

                // Required field names
                $required = array('firstName', 'lastName', 'address', 'postalCode', 'numberOfNights', 'numberOfAdults');

                // Loop over field names, make sure each one exists and is not empty
                $error = false;
                foreach($required as $field) {
                    if (empty($_POST[$field])) {
                        $error = true;
                    }
                }

                //If boolean flag is tripped get user to return to the previous page.
                if ($error) {
                    echo "<p class=\"text-warning\">You must enter a value for each required field. Required fields are marked by an asterisk (*)</p>";
                    echo "<a href='bookRoom.php'>Return to previous page</a>";

                    //Else display information
                } else {
                    echo "<p>Name: ".$firstName." ".$lastName."</p>";
                    echo "<p>Address: ".$address.", ".$postalCode."</p>";
                    echo "<p>Number of Adults: ".$adults."</p>";

                    //If children textbox is empty give it the value of 0
                    if ($children == ""){
                        $children = 0;
                    }

                    echo "<p>Number of Children: ".$children."</p>";

                    //If adults + children is greater than 8, display a message.
                    if (((int)$adults + (int)$children) > 8){
                        echo "<p>Note: You have a large party. Protip: The bathtub can double as an extra bed!</p>";
                    }

                    //if the pet option is selected, add a charge and display message.
                    if (isset($_POST['petOption'])){
                        $petOption = 50;
                        echo "<p>A $50 charge has been added to your bill for a pet friendly room.</p>";
                    }

                    //Calculations
                    $totalForNights = (int)$nights * 100;
                    $totalDueBeforeTax = $totalForNights + $petOption;
                    $totalDueAfterTax = $totalDueBeforeTax * SALES_TAX;
                    $totalTaxAmount = $totalDueAfterTax - $totalDueBeforeTax;

                    //Table for displaying charges
            ?>
                    <br>
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th scope="row">Number of nights:</th>
                            <td><?php echo "$".number_format($totalForNights, 2);?></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Pet Friendly Charge:</th>
                            <td><?php echo "$".number_format($petOption, 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row">Provincial Sales Tax:</th>
                            <td><?php echo "$".number_format($totalTaxAmount, 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row">Total:</th>
                            <td><?php echo "$".number_format($totalDueAfterTax, 2);?></td>
                        </tr>
                        </tbody>
                    </table>
            <?php
                    //Set default timezone and display when the order was placed.
                    date_default_timezone_set('America/Halifax');
                    echo "<p id='date'>Booking created at ".date('g:ia')." on ".date('F j, Y').".</p>";
                }
            ?>
        </div>
    </body>
</html>
