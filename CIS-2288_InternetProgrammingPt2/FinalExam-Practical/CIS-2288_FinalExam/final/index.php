<?php
/**
 * Author: David Hawkes
 * Co-Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This is the home page for the cis blog. It displays various news stories.
 */

	$pageTitle = "News - Home";
	include ("incPageHead.php");
	include ("connect.php");

    //Default query to get all the news story information
    $query = "SELECT * FROM news";

    //Execute the query
    $result = $db->query($query);

?>
<div class="jumbotron"></div>
<?php
        //Check to see if the query has any results
        if ($result->num_rows > 0){

            while($row = $result->fetch_assoc()){

                // if logged in do this
                if($_SESSION['loggedIn'] == false){

                    $glyphEditIcon = "";

                //else
                } elseif ($_SESSION['loggedIn'] == true) {

                    $glyphEditIcon = "<span style='float:right'><a title='edit this story' href='editNews.php?id=".$row['storyId']."'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a></span>";
                }

                echo "<div class='panel panel-default'>";
                echo "<div class='panel-heading'>".$row['headline'].$glyphEditIcon."</div>";
                echo "<div class='panel-body'>".$row['storyDetails']."</div>";
                echo "</div>";
            }

        }  else {
            //If there are no results that match the query.
            echo "<p>There are no results to display.</p>";
        }

	include ("incPageFoot.php");
?>