<?php
/**
 * Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This page will display footer information for the cis blog as well as close all html tags.
 */
?>
				<footer class="footer">
					<p>&copy; 2018 CIS PROductions - u: admin p: news2288</p>
				</footer>
				<!-- jQuery library -->
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
				<!-- Latest compiled JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				</div> <!-- close col -->
			</div> <!-- close row -->
		</div> <!-- close container -->
	</body>
</html>