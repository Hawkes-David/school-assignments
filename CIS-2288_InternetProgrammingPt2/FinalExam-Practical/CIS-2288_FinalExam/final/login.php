<?php
/**
 * Author: David Hawkes
 * Co-Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This page will allow a user to log into the cis blog as an admin.
 */

	$pageTitle = "News - Login";
	include ("incPageHead.php");

    //If the user has not logged in...
    if ($_SESSION['loggedIn'] == false){

        //and the submit button has been clicked check the credentials.
        if (isset($_POST["username"])){

            //The user entered the correct credentials
            if ($_POST["username"] == "admin" && $_POST["password"] == "news2288"){
                $_SESSION['loggedIn'] = true;
                $_SESSION['username'] = $_POST["username"];

                //redirect to the index.php page
                header('location: index.php');
                exit();

                //The user has entered incorrect credentials
            } else {
                $error = "Bad credentials.";
            }
        }
    }
?>
        <form action="login.php" method="post">
			<div class="form-group">
				<label for="user">Username:</label>
				<input type="text" name="username" id="user" class="form-control"/><br>
				<label for="password">Password:</label>
				<input type="password" id="password" name="password" class="form-control"/><br>
				<input type="submit" name="submit" value="Login" class="btn btn-default"/>
			</div>
        </form>
        <?php
            if (isset($error)) {
                echo "<div class='alert alert-danger'>$error</div>";
            }

			include ("incPageFoot.php");
        ?>


