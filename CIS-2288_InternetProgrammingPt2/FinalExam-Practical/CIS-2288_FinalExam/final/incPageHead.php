<?php
/**
 * Author: David Hawkes
 * Co-Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This page will record and display all header information required for the cis blog page
 */

//start the session
session_start();

//Set the session variables if they are not set.
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}


?>
<!doctype html>
<html lang="en">
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css">
		<link href="css/jumbotron-narrow.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet" />
        <title><?php echo $pageTitle; ?></title>
    </head>
	<body>
		<div class="container">
			<div class="header clearfix">
				<nav>
					<ul class="nav nav-pills pull-right">
						<li role="presentation"><a href="index.php">Home</a></li>
                        <!-- Both buttons below should never be visible at the same time -->
                        <?php
                            if ($_SESSION['loggedIn'] == false){
                                echo "<li role='presentation'><a href='login.php'>Login</a></li>";
                            } elseif ($_SESSION['loggedIn'] == true){
                                echo  "<li role='presentation'><a href='logout.php'>Logout</a></li>";
                            }
                        ?>
					</ul>
				</nav>
				<h3 class="text-muted"><a href="index.php">The CIS Blog</a></h3>
			</div>
			<div class="row marketing">
				<div class="col-lg-12">