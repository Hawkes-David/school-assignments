<?php
/**
 * Author: David Hawkes
 * Co-Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This page will allow the user to edit a new story.
 */

	$pageTitle = "News - Edit";
	include ("incPageHead.php");
    include ("connect.php");


    //If the user is not logged or is logged in but tried to access the page without clicking an edit link in redirect them to the index.php
    if ($_SESSION['loggedIn'] == false || !isset($_GET['id'])){

        //redirect to the index.php page
        header('location: index.php');
        exit();
    }

echo "<body>";
    echo "<h2>Edit News Item</h2>";

    //If the user has clicked the submit button
    if (isset($_POST['id'])){

        //This will get the ID for the news item that the user would like to edit
        $id = $_POST['id'];

        //Find the lengths of the attributes, this will be used for validation
        $headlineLength = strlen($_POST['headline']);
        $storyLength = strlen($_POST['storyDetails']);


        //Validate the data entered by the user
        if ($headlineLength == 0 || $storyLength == 0){

            //Set the user message
            $class = "class='alert alert-danger'";
            $userMessage = "<p>One or more fields was empty or there was a problem with your query <a href='editNews.php?id=".$id."'>Go Back</a></p>";

        } else {

            //Query to edit the story of choice
            $query = "UPDATE news SET headline=?, storyDetails=? WHERE storyId=".$id;

            //The prepared statement and execution of the query
            if($statement = $db->prepare($query)){
                $statement->bind_param("ss", $_POST['headline'], $_POST['storyDetails']);
                $statement->execute();
                $statement->close();

                //Set the user message
                $class = "class='alert alert-success'";
                $userMessage = "<p>Edit Success <a href='index.php'>View All News</a></p>";
            }
        }

        //Display the user message
        echo "<div ".$class.">$userMessage</div>";

        //disconnect the connection to the DB
        $db->close();


    } elseif (isset($_GET['id'])){

        //Get the ID that the user would like to edit
        $id = ($_GET['id']);

        //Query to find all attributes related to that ID
        $query = "SELECT * FROM news WHERE storyId = ?";

        //The prepared statement and execution of the query
        if($statement = $db->prepare($query)){
            $statement->bind_param("s", $id);
            $statement->execute();
            $result = $statement->get_result();
            $statement->close();
        }

        //Fetch the result if one exists
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
        }

        //this frees up memory on the server
        $result->free();

        //disconnect the connection to the DB
        $db->close();

        ?>

        <form action="editNews.php?id=<?php echo $row['storyId'] ?>" method="post">
            <div class="form-group">
                <label for="headLine">Headline:</label><br>
                <input id="headLine" type="text" name="headline" class="form-control" value="<?php echo $row['headline']?>" />
            </div>
            <div class="form-group">
                <label for="storyDetails">Story Details:</label><br>
                <textarea id="storyDetails" class="form-control" name="storyDetails"><?php echo $row['storyDetails']?></textarea><br>
                <input type="hidden" name="id" value="<?php echo $row['storyId']?>" />
                <input type="submit" class="btn btn-default" value="Commit Edit">
            </div>
        </form>

        <?php
    }

    include ("incPageFoot.php");

?>
