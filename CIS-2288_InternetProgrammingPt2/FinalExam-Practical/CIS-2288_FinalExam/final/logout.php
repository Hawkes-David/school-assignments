<?php
/**
 * Author: David Hawkes
 * Co-Author: Don Bowers
 * Date: 2018-12-11
 * Purpose: This page will allow a user to log out of the cis blog
 */
    //End the session
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    //redirect to the index.php page
    header('location: index.php');
?>
