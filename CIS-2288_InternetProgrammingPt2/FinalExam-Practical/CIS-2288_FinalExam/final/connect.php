<?php
/**
 * Author: David Hawkes
 * Date: 2018-12-11
 * Purpose:This page will allow a connection to the news2288 database.
 */
    //If everything is valid, make a connection to the database
    @ $db = new mysqli('localhost', 'root', '', 'news2288');

    // if mysqli_connect_errno() is set, we did not successfully connect. Here we deal with the error.
    if (mysqli_connect_errno()) {
        echo 'Error: Could not connect to database.  Please try again later.';
        exit;
    }
?>