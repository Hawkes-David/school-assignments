<?php
/**
 * Author: David Hawkes
 * Date: 2018-09-16
 * Purpose: The recreation of a web page snippet to refresh css and html skills as well as introduce php tags and echos.
 */
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php echo "Refresh Me!"; ?></title>
        <link rel="stylesheet" href="css/refreshMeStyles.css">
    </head>
    <body>
        <div id="container">
            <?php echo "<h2 id='reality'>Back to reality!</h2>"; ?>
            <p>This particular section is a paragraph. It appears as a paragraph because it takes up the width of the section (block level) and there is a small amount of space above and below. Paragraphs can contain other 'inline' elements like links and spans. They cannot contain other block level elements like lists or images.</p>
            <?php echo "<h2 id='list'>My List</h2>"; ?>
            <?php //Unordered list. ?>
            <ul>
                <li>Keep the whitespace to a minimum. We all want lean, clean code.</li>
                <?php echo "<li>How you indent matters.</li>"; ?>
                <li>Please don't forget your comments.</li>
            </ul>
            <?php echo "<h2 id='dnds'>Do's and Don'ts</h2>"; ?>
            <?php //Reversed list, containing several italicized words and one rainbow coloured word ?>
            <ol reversed>
                <li>Don't forget to communicate with me. <span class="italicizedWords">Communication</span> is an essential part of learning. Skype, email, Google Hangout, telephone...</li>
                <li><span class="italicizedWords">Do</span> read your textbook. It's easy and important.</li>
                <li><span class="italicizedWords">Do</span> - Practice, practice, practice - then follow up with me for help if required. You need to ask questions when you don't understand things.</li>
                <li><span class="italicizedWords">Do</span> - Plan ahead! Do not leave things until the last minute.</li>
                <li><span class="italicizedWords">Don't</span> expect this course to be a <span id="rainbow"><span class="redLetter">w</span><span class="blueLetter">a</span><span class="greenLetter">l</span><span class="purpleLetter">k</span></span> in the park. It will take time, effort, and mental fitness to succeed!</li>
            </ol>
            <?php
            //Sets the default time zone to New York (Eastern time) and displays the date and time.
            date_default_timezone_set('America/New_York');
            echo "<p id='date'>Today is ".date('F j, Y').". The current time in New York City is ".date('h:i A').".</p>";
            ?>
            <?php //Creates the mystery box. ?>
            <div id="mysteryBox">
                <p>4, 8, 15, 16, 23 42</p>
            </div>
        </div>
    </body>
</html>
