# School Assignments

This repository exists to showcase deliverables for the Computer Information Systems program at Holland College. Holland College offers a suite of web development courses which consist of the following:
- CIS-1280 Web Application Development (HTML & CSS)
- CIS-2286 Internet Programming Pt. 1 (JavaScript)
- CIS-2288 Internet Programming Pt. 2 (PHP)
I have limited the content to CIS-2288 Internet Programming Pt. 2 for two reasons. Firstly, CIS-2288 is the most recent web development course I have taken and feel it best represents my abilities as a programmer today. CIS-2288 incorporates all the skills learned from its predecessors. Secondly, as CIS-1280 and CIS-2286 were first-year courses, I no longer have access to the requirements for the deliverables for those courses. Without the requirements, I feel it would be difficult to get a sense of the learning that had taken place and what specifically was being asked of the student.
